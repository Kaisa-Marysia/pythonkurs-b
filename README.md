# Pythonkurs B wie Bython

Pythonkurs für absolute Anfänger*innen and terrified beginners ab August 2022

Der Kurs wird von der Wau-Holland-Stiftung gefördert.
![](https://wauland.de/images/logo-blue.svg)

Hier finden sich Aufgaben, Hinweise und weitere Ressourcen für den Python-Kurs für absolute Anfänger*innen von Piko. Der Kurs läuft von August 2022 bis November 2022 und soll die ersten Schritte im Programmieren vermitteln.
Mehr Informationen auf https://www.haecksen.org/

# Erster Termin: 17.08.


:alert:
**Am 21.09. fängt der Python-Kurs erst um 20:00 an!**