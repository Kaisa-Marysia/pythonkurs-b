# Pikos Python Kurs am 14.09.2022

**Am 21.09. fängt der Python-Kurs erst um 20:00 an!**

## Protokoll
Bython 14.09.22


1. Hausaufgaben-Check

    • Kreativaufgabe: Baut eine Variante von dem Regenbeispiel


Optional: sinnvoll beim programmieren: print(Variablen)
        ◦ kann zum testen verwenden, kleinere Fehler, Interface nicht schön o.ä., als Kommentar drin lassen für spätere Verwendung, damit man rechtzeitig sieht ob das Geschriebene so funktioniert wie man das möchte
        ◦ Wenn man einen Tupel aus einem einzelnen String o.ä. machen möchte, weil n 	  Tupel verlangt wird, kann man das in Klammern setzen und ein Komma dahinter   machen. 
Das wird ein Tupel:  („Katze“, )

 
2. Einrückungen Part 2: 

while: „solange“ 
while True: - solange X wahr ist, führ folgendes aus
while not – solange X nicht erfüllt ist/zutrifft, führ folgendes aus

while-Schleifen:  Führen Dinge aus, bis ein definiertes Endkriterium erreicht ist. Solange das nicht der Fall ist, wird was ausgeführt. 

While-Schleifen-Beispiel: Obstschalen-Füll-Automat: Solange „voll“ nicht True ist, print(Inhalt der Obstschale) und füge 1 Apfel hinzu. Wenn Anzahl der Elemente in Obstschale >10, wird Variabel „voll“ mit True definiert.
So lange (while) der Obstkorb nicht voll ist, werden Äpfel hinzugefügt. Sobald voll = True ist, hört die Schleife auf und keine Äpfel werden mehr hinzugefügt.


## Pads
Namenssuche:  
https://md.ha.si/avA5mCE0QBuITFR_w2Jl-Q#  
Regenbeispiele:  
https://md.ha.si/Zi_4cvX7R_OdeWjgFgUt6Q?view


## Pikos Datei

### Hausaufgaben
Eine Lösung für Aufgabe 2.1 (gute Lösungen können auch anders aussehen!)
"Lasst ein Programm nach dem Alter fragen und wenn es über 18 ist, begrüßt es die User*innen; bei unter 18 sagt es, sie seien zu jung..."
```python
alter = int(input("Wie alt bist du? "))
if alter >= 18:
    print("Hallo, willkommen!")
else:
    print("Du bist leider zu jung!")
```

```python
es_laechelt = True
es_liegt_auf_einem_Regenbogen = True

if es_laechelt or es_liegt_auf_einem_Regenbogen: # => True
    print("Wunderbar, alles geht sowieso nicht!")
if es_laechelt and es_liegt_auf_einem_Regenbogen: # => True
    print("Jetzt wachsen mir Flügel!.")
    
# and wird nur dann True, wenn alle beide True sind.
# or wird dann True, wenn mindestens eins der beiden True ist.
# or ist *nicht* das deutsche Entweder-Oder
```
```python
geheimes_wort = "Luftschloss"
angezeigtes_wort = "___________"

versuch = input("Rate einen Buchstaben in diesem Wort: " + angezeigtes_wort)
# erstmal sichergehen, dass es nur ein Buchstaben war
if len(versuch) > 1:# Länge von erster_versuch größer als 1
    print("Bitte gib nur einen Buchstaben ein!")
else:
    if versuch in geheimes_wort: # Das ist erlaubt! "in" ist ein "membership operator". Das Gegenteil ist "not in"...
        print("Der Buchstabe kommt im Wort vor.")
    else:
        print("Dieser Buchstabe kommt leider nicht vor, versuche es noch einmal")
```
```python
n_gaeste = 5
n_personen = n_gaeste + 1
n_kuchenstuecke = input("Wie viele Kuchenstücke? ")
n_kekse = input("Wie viele Kekse? ")

# print(n_kuchenstuecke, n_kekse)


if (int(n_kuchenstuecke) % 6 == 0 and int(n_kuchenstuecke) > 0) or int(n_kekse) > 30:# genug kekse/kuchen
    print("Die Party kann losgehen!")
else:
    print("Problem entdeckt! Falsche Anzahl an Snacks!")
```
### Einrückungen und Schleifen

```python
# eine while-Schleife: Ein Obstschalen-Füll-Automat


passtnochwasrein = True

obstschale = ("apfel", "apfel", "banane", "kokosnuss")

while passtnochwasrein:
    print("Inhalt der Obstschale:", obstschale)
    obstschale = obstschale + ("apfel",)
    print("Habe was reingetan!")
    if len(obstschale) >= 10:
        passtnochwasrein = False
        print("Jetzt ist aber voll!")
    print("Länge der Obstschaleninhaltstupels: ", len(obstschale))
    
print("Hier geht es weiter!")
```